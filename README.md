# kerberos-k8s-image

This repository defines the `kerberos-kinit` docker image build process. This image is a simple Debian base layer on top of which we install some kerberos-related tooling. It will be used in Kubernetes, to initialize and renew Kerberos tokens associated to Pods.
